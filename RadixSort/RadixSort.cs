﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RadixSort
{
    public class RadixSort
    {
        public int range { get; set; } = 10;
        public ArrayList[] makeGroups(int[] array, int step)
        {
            ArrayList[] group = new ArrayList[range];
            for (int i = 0; i < range; ++i)
                group[i] = new ArrayList();

            for (int i = 0; i < array.Length; ++i)
            {
                int temp = (array[i] % (int)Math.Pow(range, step + 1)) /
                    (int)Math.Pow(range, step);
                group[temp].Add(array[i]);

            }
            return group;
        }

        public int[] SortByGroups(ArrayList [] group)
        {
            int k = 0;
            int[] array = new int[range];
            for (int i = 0; i < group.Length; ++i)
                if (group[i].Count != 0)
                {
                    for (int j = 0; j < group[i].Count; ++j)
                    {
                        array[k++] = (int)group[i][j];
                    }
                }
            return array;
        }
        public int[] Sort(int[] array, int range)
        {
            for (int step = 0; step < countMaxStep(array); step++ )
            {
                array = SortByGroups(makeGroups(array, step));
            }
            return array;
        }
        public int countMaxStep(int [] array)
        {
        int maxCount = 0;
        for(int i = 0; i <array.Length; i++)
        {
            int count = (array[i] == 0) ? 1 : 0;
            int tmp = array[i];
            while (array[i] != 0)
            {
                count++;
                array[i] /= 10;
            }
            if (count > maxCount)
                maxCount = count;
        }
        return maxCount;
    }
    
    }
}
