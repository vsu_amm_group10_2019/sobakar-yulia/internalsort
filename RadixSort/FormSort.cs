﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RadixSort

{
  
    public partial class FormSort : Form
    {
        public RadixSort radixSort = new RadixSort();
        public FormSort()
        {
            InitializeComponent();
        }

        private void buttonSort_Click(object sender, EventArgs e)
        {
            buttonSort.Text = "OK";
            buttonSort.Enabled = false;
            buttonSort.Refresh();
            listViewPrimary.Clear();

            int[] arr = new int[10];
            Random random = new Random();
            for (int i = 0; i < arr.Length; i++)
            {
                arr[i] = random.Next(0, 999);
                listViewPrimary.Items.Add(arr[i].ToString());
                listViewPrimary.Refresh();
            }
            Thread.Sleep(500);

            for (int steps = 0; steps < 3; steps++)
            {
                ArrayList[] groupRadix = radixSort.makeGroups(arr, steps);
                labelGroup.Text = $"Группировка по {steps + 1}-му разряду";
                switch (steps)
                {
                    case 0:
                        labelGroup.ForeColor = Color.Red;
                        break;
                    case 1:
                        labelGroup.ForeColor = Color.Blue;
                        break;
                    default:
                        {
                            labelGroup.ForeColor = Color.Green;
                        }
                        break;
                }
                labelGroup.Refresh();

                arr = radixSort.SortByGroups(groupRadix);
                PrintToGridView(dataGridViewGroup, groupRadix);
                PrintToListView(listViewSorted , groupRadix);
            }
            buttonSort.Text = "Ещё раз";
            buttonSort.Enabled = true;
        }

        private int rowsMaxCount(ArrayList[] group)
        {
            int maxCount = 0;
            for (int i = 0; i < group.Length; ++i)
                if (group[i].Count > maxCount)
                    maxCount = group[i].Count; 
            return maxCount;

        }

        private void PrintToGridView(DataGridView dataGridView, ArrayList[] group)
        {
            dataGridView.Rows.Clear();
            dataGridViewGroup.RowCount = rowsMaxCount(group);

            for (int i = 0; i < group.Length; ++i)
                if (group[i].Count != 0)
                {
                    for (int j = 0; j < group[i].Count; ++j)
                    {
                        dataGridView.Rows[j].Cells[i].Value = group[i][j];
                        dataGridView.Refresh();
                        Thread.Sleep(200);
                    }
                }
        }
        private void PrintToListView(ListView listView, ArrayList[] array)
        {
            listView.Items.Clear();
            listView.Refresh();
            for (int i= 0; i < array.Length; i++)
                for(int j = 0; j <array[i].Count; j++)
            {
                listView.Items.Add(array[i][j].ToString());
                listView.Refresh();
                Thread.Sleep(200);
            }

        }
    }
}
